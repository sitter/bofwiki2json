<!--
    SPDX-License-Identifier: CC0-1.0
    SPDX-FileCopyrightText: 2019-2020 Harald Sitter <sitter@kde.org>
-->

Converts bof wiki pages to json blobs.

Various gem dependencies need installing first.
Easiest to do by running `bundle install`.

Also requires the CLI tool pandoc to be installed on the system.

Output is in bof.json.

Download at
https://invent.kde.org/sitter/bofwiki2json/-/jobs/artifacts/master/raw/bof.json?job=bof2wiki
