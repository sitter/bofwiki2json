#!/usr/bin/env ruby

# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2019-2022 Harald Sitter <sitter@kde.org>

require 'jsoner'
require 'nokogiri'
require 'open-uri'
require 'pandoc-ruby'

CURRENT_YEAR = Time.now.year
DAYS = %w[Monday Tuesday Wednesday Thursday Friday].freeze

STDOUT.sync = true

unless system('which pandoc', %i[out err] => '/dev/null')
  raise "Pandoc doesn't seem to be installed!"
end

# Determine which year to use. This has logic to determine whether to use the
# previous year as the current year will only get bof pages in Q2 or so, when
# akademy prep ramps up.
def determine_year(year = CURRENT_YEAR)
  begin
    open("https://community.kde.org/index.php?action=raw&title=Akademy/#{year}/Monday")
    return year
  rescue => e
    now = Time.now
    if (year == CURRENT_YEAR && now.month < 6) || (now.year == 2022 && now.month < 8)
      # Try past year until June, if we don't have a new page by June this needs
      # looking into.
      return determine_year(CURRENT_YEAR - 1)
    end

    raise "Year page doesn't exist :O"
  end
end

YEAR = determine_year

# Final output will be
# { day => { room => [{"Time" => 123, "Subject"=>...} ...] } }
output = {}

DAYS.each do |day|
  output[day] = {}
end

def url(day)
  "https://community.kde.org/index.php?action=raw&title=Akademy/#{YEAR}/#{day}"
end

def retry_open(url)
  5.times do
    begin
      return open(url)
    rescue => e
      warn "Failed to download -> waiting and retrying\n#{e}"
      sleep 2
    end
  end
end

DAYS.each do |day|
  puts "======================= #{day} ======================="
  io = retry_open(url(day))
  pp io
  wiki_data = io.read
  raise 'Failed to get any data!' if !wiki_data || wiki_data.empty?

  converter = PandocRuby.new(wiki_data, from: :mediawiki, to: :html)
  html = converter.convert
  puts html

  doc = Nokogiri::HTML(html)
  raise doc.errors unless doc.errors.empty?

  doc.xpath('//table').each do |table|
    puts 'Processing table...'

    # The tables are a technical shambles.
    # The head is a single column with the room name.
    # The first row is the definition of heads.
    # Everything after that is the actual content.
    # Somewhat fortunately jsoner fails to process this properly!

    # The first the we can find -> room name
    room = table.at_xpath('.//thead//th').content
    raise "Couldn't find room" unless room && !room.empty?

    # Throw out the previous thead's tr and replace it with a new tr converted
    # from the first row of the tbody. This makes the table well-formed under
    # jsoners expectations (only a single header row now containing the room name)
    table.at_xpath('.//thead/tr').remove
    header_row = table.at_xpath('.//tr')
    header_row.remove
    header_row.xpath('.//td').each do |td|
      td.node_name
      td.node_name = 'th'
    end
    table.at_xpath('./thead').add_child(header_row)

    json = Jsoner.parse(table.to_s)
    data = JSON.parse(json)
    if data.empty?
      pp table.to_s
      pp data
      raise 'Something seems to have gone mighty wrong. The json data is empty'
    end

    # Would be nicer to unpack the array into objects associated with
    # time keys, but given the conference json doesn't do that either I suppose
    # we'll be fine
    output[day][room] = data
  end

  next unless output[day].empty?
  next if YEAR != '2022' && day == 'Thursday' # Thursdays are trainings. Disable the error.

  pp output
  pp doc.to_s
  raise "Failed to produce room data for #{day}, something went terribly wrong!"
end

File.write('bof.json', JSON.generate(output, pretty_generate: true))
